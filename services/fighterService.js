const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    // TODO: Implement methods to work with fighters //DONE
    search(search) {
        const item = FighterRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }

    getAll() {
        const allItems = FighterRepository.getAll();
        return Object.keys(allItems).length === 0 
            ? null
            : allItems;
    }

    createFighter(newFighter) {
        return FighterRepository.create(newFighter);
    }

    updateFighter(id, dataToUpdate) {
        return FighterRepository.update(id, dataToUpdate);
    }

    deleteFighter(id) {
        return FighterRepository.delete(id);
    }
}

module.exports = new FighterService();