const { UserRepository } = require('../repositories/userRepository');

class UserService {

    // TODO: Implement methods to work with user //DONE

    search(search) {
        const item = UserRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }

    getAll() {
        const allItems = UserRepository.getAll();
        return allItems;
    }

    createUser(newUser) {
        return UserRepository.create(newUser);
    }

    updateUser(id, dataToUpdate) {
        return UserRepository.update(id, dataToUpdate);
    }

    deleteUser(id) {
        return UserRepository.delete(id);
    }
}

module.exports = new UserService();