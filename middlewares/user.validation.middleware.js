const { user } = require('../models/user');
const nameRegExp = /^[A-Z][A-Za-z-]{1,15}$/gm;
const emailRegExp = /^([A-z][-\w.]{3,15})+(@gmail.com)$/gm;
const phoneRegExp = /^(\++380)+(\d{9})$/gm;
const passwordRegExp = /^(?=.*\d)(?=.*[A-z])[A-z0-9-_.]{4,15}$/gm;

const createUserValid = (req, res, next) => {
    req.isValid = true;
    let { firstName = '', lastName = '', email = '', phoneNumber = '', password = '' } = req.body;

    if ( 
        firstName.match(nameRegExp) === null 
        || lastName.match(nameRegExp) === null
        || email.match(emailRegExp) === null
        || phoneNumber.match(phoneRegExp) === null
        || password.match(passwordRegExp) === null
    ) {
        req.isValid = false;
    }  
    req.newUser = {firstName, lastName, email, phoneNumber, password};
    next();
}

const updateUserValid = (req, res, next) => {
    req.isValid = true;
    let { firstName = '', lastName = '', email = '', phoneNumber = '', password = '' } = req.body;
    let allParams = { firstName, lastName, email, phoneNumber, password };
    let newParams = {};
     (Object.entries(allParams).filter( ([key, val]) => val !== ''))
                    .forEach( ([key, val]) => newParams[key] = val);
    

    if ( 
        (firstName.match(nameRegExp) === null && firstName !== '')
        || (lastName.match(nameRegExp) === null && lastName !== '')
        || (email.match(emailRegExp) === null && email !== '')
        || (phoneNumber.match(phoneRegExp) === null && phoneNumber !== '')
        || (password.match(passwordRegExp) === null && password !== '')
    ) {
        req.isValid = false;
    }  
    req.newParams =  newParams;
    next();
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;