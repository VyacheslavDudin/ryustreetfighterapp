const { fighter } = require('../models/fighter');
const nameRegExp = /^[A-Z][A-Za-z-]{1,15}$/gm;

const createFighterValid = (req, res, next) => {
    req.isValid = true;
    
    let { name = '', health, power, defense } = req.body;
    let newFighter = {name, health, power, defense};

    if (!propertiesIsValid('create', newFighter)) {
        req.isValid = false;
    }  
    req.newFighter = newFighter;
    next();
}

const updateFighterValid = (req, res, next) => {
    req.isValid = true;
    let { name = '', health, power, defense, ...rest } = req.body;
    if(rest.length !== 0) {
        req.isValid = false;
        next();
    }
    let allFighterParams = {name, health, power, defense};
    let newFighterParams = {};
     (Object.entries(allFighterParams).filter( ([, val]) => val !== '' && val !== undefined ))
                    .forEach( ([key, val]) => newFighterParams[key] = val);
    

    if (!propertiesIsValid('update', newFighterParams)) {
        req.isValid = false;
    }  
    req.newFighterParams = newFighterParams;
    next();
}

const propertiesIsValid = (action, properties) => {
    let { name = '', health, power, defense } = properties;

    if(
        (
        name.match(nameRegExp) !== null 
        && health > 0
        && health < 1000
        && !isNaN(health)
        && power > 0
        && power < 100
        && !isNaN(power) 
        && defense > 0
        && defense < 10
        && !isNaN(defense)
        && (action === 'create')
        )
        ||
        (
            (name.match(nameRegExp) !== null || name === '')
            && ((health > 0 && health < 1000 && !isNaN(health)) || health === undefined)
            && ((power > 0 && power < 100 && !isNaN(power)) || power === undefined)
            && ((defense >= 0 && defense <= 10 && !isNaN(defense)) || defense === undefined)
            && (action === 'update')
        )
    ) return true;
    else return false;
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;