const { Router } = require('express');
const AuthService = require('../services/authService');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.post('/login', (req, res, next) => {
    try {
        let { email = '', password = '' } = req.body;
        let data = { email, password };
        req.result = JSON.stringify(AuthService.login(data));
        console.log(AuthService.login(data));
        console.log(req.result);
    } catch (err) {
        res.err = err;
        console.log(res.err);
    } finally {
        next();
    }
}, responseMiddleware);

module.exports = router;