const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.get('/', function (req, res, next) {
    req.result = UserService.getAll();
    req.entityName = 'User';
    next();
}, responseMiddleware);

router.get('/:id', function (req, res, next) {
    req.result = UserService.search(req.params.id);
    req.entityName = 'User';
    next();
}, responseMiddleware);


router.post('/', createUserValid, function (req, res, next) {
    req.entityName = 'User';
    if(req.isValid !== false) 
        req.result = UserService.createUser(req.newUser);
    next();
}, responseMiddleware);


router.put('/:id', updateUserValid, function (req, res, next) {
    req.entityName = 'User';
    if(req.isValid !== false) 
        req.result = UserService.updateUser(req.params.id, req.newParams);
    next();
}, responseMiddleware);

router.delete('/:id', function (req, res, next) {
    req.entityName = 'User';
    req.result = UserService.deleteUser(req.params.id);
    next();
}, responseMiddleware);

module.exports = router;