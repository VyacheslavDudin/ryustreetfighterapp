const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

router.get('/', function (req, res, next) {
    req.result = FighterService.getAll();
    req.entityName = 'Fighter';
    next();
}, responseMiddleware);

router.get('/:id', function (req, res, next) {
    req.result = FighterService.search(req.params.id);
    req.entityName = 'Fighter';
    next();
}, responseMiddleware);


router.post('/', createFighterValid, function (req, res, next) {
    req.entityName = 'Fighter';
    if(req.isValid === true) 
        req.result = FighterService.createFighter(req.newFighter);
    next();
}, responseMiddleware);


router.put('/:id', updateFighterValid, function (req, res, next) {
    req.entityName = 'Fighter';
    if(req.isValid === true) 
        req.result = FighterService.updateFighter(req.params.id, req.newFighterParams);
        console.log("nFP", req.newFighterParams);
    next();
}, responseMiddleware);

router.delete('/:id', function (req, res, next) {
    req.entityName = 'Fighter';
    req.result = FighterService.deleteFighter(req.params.id);
    next();
}, responseMiddleware);

// TODO: Implement route controllers for fighter //done

module.exports = router;